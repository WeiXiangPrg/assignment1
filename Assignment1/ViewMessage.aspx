﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewMessage.aspx.cs" Inherits="Assignment1.Message" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style1">
        <strong>Messages</strong></p>
    <p class="auto-style1">
        <asp:GridView ID="gvMessage" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gvMessage_RowCommand" Width="1088px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="MessageID" DataNavigateUrlFormatString="ReplyForm.aspx?messageid={0}" Text="Reply" NavigateUrl="~/ReplyForm.aspx"  ControlStyle-CssClass="ui primary button"/>
                <asp:BoundField DataField="MessageID" HeaderText="MessageID" />
                <asp:BoundField DataField="ParentName" HeaderText="From" />
                <asp:BoundField DataField="Name" HeaderText="To" />
                <asp:BoundField DataField="DateTimePosted" HeaderText="Posted On" />
                <asp:BoundField DataField="Text" HeaderText="Message" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <br />
        &nbsp&nbsp
        <br />
    </p>
</asp:Content>

﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MentorMenu.ascx.cs" Inherits="Assignment1.MentorMenu" %>
<!-- A grey navbar that expands horizontally at medium device -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- The brand(or icon) of the navbar -->
    <a class="navbar-brand" href="Mentor.aspx"
        style="font-size:32px; font-weight:bold; color:#808080;">
        ABC Polytechnic
    </a>
    <!-- Toggle/collpsible Button, also known as hamburger button -->
   <button class="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#ABCNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

                
    <!--Links in the navbar, displayed as drop-down list when collasped -->
    <div class="collapse navbar-collapse" id="ABCNavbar">
        <!-- Links that are aligned to the left, 
            mr-auto: right margin auto-adjudted -->

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="ApprovePortfolio.aspx">Approve/Disapprove</a>

                
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Search.aspx">Search</a>

                
            </li>
            <li class="nav-item">
                <a class="nav-link" href="PostSuggestion.aspx">Post Suggestion</a>
                </li>
             <li class="nav-item">
                <a class="nav-link" href="ViewMessage.aspx">View e-message</a>
                </li>
                        
            <li class="nav-item">
                <a class="nav-link" href="ChangePass.aspx">Change Password</a>
                </li>
           

          
        </ul>
        <!-- Links that are aligned to the right, ml-auto: left margin auto-adjusted -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- A Web form control button for logging our user -->
                  <i class="power off icon"></i>
                   <asp:Button ID="btnLogOut" runat="server" Text="Log Out"  CausesValidation="false"
                CssClass="negative ui button" OnClick="btnLogOut_Click"/>
            </li>
         
        </ul>
    </div>
</nav>
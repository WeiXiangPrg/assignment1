﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class UpdatePort : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmailAddr"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                lblID.Text = Request.QueryString["projectid"];
                lblName.Text = Request.QueryString["title"];
                txtName.Text = lblName.Text;
                //Displaying the Description and ProjectURL
                string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
                SqlConnection conn = new SqlConnection(sqlConn);
                SqlCommand cmd = new SqlCommand("SELECT Description FROM Project WHERE ProjectID = '" + lblID.Text + "'", conn);
                SqlCommand cmd2 = new SqlCommand("SELECT ProjectURL FROM Project WHERE ProjectID = '" + lblID.Text + "'", conn);
                SqlCommand cmdReflect = new SqlCommand("SELECT Reflection FROM ProjectMember WHERE ProjectID = '" + lblID.Text + "' " +
                    "AND StudentID = '" + Session["StudentID"] + "'", conn);
                SqlDataAdapter daProject = new SqlDataAdapter(cmd);
                DataSet projectDesc = new DataSet();
                SqlDataAdapter daProject2 = new SqlDataAdapter(cmd2);
                DataSet projectURL = new DataSet();
                SqlDataAdapter daReflect = new SqlDataAdapter(cmdReflect);
                DataSet projectReflect = new DataSet();
                //Open connection
                conn.Open();
                daProject.Fill(projectDesc, "Description");
                daProject2.Fill(projectURL, "ProjectURL");
                daReflect.Fill(projectReflect, "Reflection");
                txtReflection.Text = projectReflect.Tables["Reflection"].Rows[0]["Reflection"].ToString();
                txtDesc.Text = projectDesc.Tables["Description"].Rows[0]["Description"].ToString();
                txtURL.Text = projectURL.Tables["ProjectURL"].Rows[0]["ProjectURL"].ToString();
                // Displaying the members in the group
                SqlCommand cmd3 = new SqlCommand("SELECT Name,ProjectMember.StudentID FROM ProjectMember INNER JOIN" +
                    " Student on ProjectMember.StudentID = Student.StudentID " +
                    "WHERE ProjectID = '" + lblID.Text + "'", conn);
                SqlDataAdapter daMember = new SqlDataAdapter(cmd3);
                DataSet projectMember = new DataSet();
                daMember.Fill(projectMember, "Name");
                List<string> members = new List<string>();
                List<string> memberID = new List<string>();
                for (int i = 0; i < projectMember.Tables["Name"].Rows.Count; i++)
                {
                    members.Add(projectMember.Tables["Name"].Rows[i]["Name"].ToString());
                    memberID.Add(projectMember.Tables["Name"].Rows[i]["StudentID"].ToString());
                }
                //Checks if the user logged in is a Member or Leader 
                SqlCommand cmd4 = new SqlCommand("SELECT Role FROM ProjectMember WHERE " +
                    "ProjectID = '" + lblID.Text + "' and StudentID = '" + Session["StudentID"] + "'", conn);
                SqlDataAdapter daRole = new SqlDataAdapter(cmd4);
                DataSet projectRole = new DataSet();
                daRole.Fill(projectRole, "Role");
                string userRole = projectRole.Tables["Role"].Rows[0]["Role"].ToString();
                if (userRole == "Leader") //Adds items into the list
                {
                    txtAdd.Visible = true;
                    txtAdd.Enabled = true;
                    lblAdd.Text = "Add Members :";
                    lblAdd2.Text = "(*Enter Members Full Name*)";
                    for (int x = 0; x < members.Count; x++)
                    {
                        ListItem mem = new ListItem(members[x], memberID[x], true);
                        if (mem.Value == Session["StudentID"].ToString())
                            mem.Enabled = false;
                        
                        mem.Selected = true;
                        cblMembers.Items.Add(mem);
                    }
                    lblMembers.Text = "Remove members by unticking them";
                }
                else
                {
                    txtAdd.Visible = false;
                    txtAdd.Enabled = false;
                    lblMembers.Text = string.Join(",", members.ToArray());
                }
                //close conneciton
                conn.Close();
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Project objProject = new Project();
                objProject.projectId = Convert.ToInt32(lblID.Text);
                objProject.description = txtDesc.Text;
                objProject.title = txtName.Text;
                objProject.projectURL = txtURL.Text;
                // Code for picture uploading
                string uploadedFile = "";
                if (upPoster.HasFile == true)
                {
                    string savePath;

                    //Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(upPoster.FileName);
                    uploadedFile = upPoster.FileName;

                    //MapPath - to fins the complete path to the images folder in server
                    savePath = MapPath("~/Projects/" + uploadedFile);
                    objProject.projectPoster = upPoster.FileName;
                    try
                    {
                        upPoster.SaveAs(savePath); //Upload the file to server
                        lblMsg.Text = "File uploaded successfully.";
                        imgPhoto.ImageUrl = "~/Projects/" + uploadedFile;

                    }
                    catch (IOException)
                    {
                        //File IO error, could due to access rights denied
                        lblMsg.Text = "File uploading fail!";

                    }
                    catch (Exception ex) // other type of error
                    {
                        lblMsg.Text = ex.Message;
                    }
                }
                else
                {
                    string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
                    SqlConnection conn = new SqlConnection(sqlConn);
                    SqlCommand cmd3 = new SqlCommand("SELECT ProjectPoster FROM Project WHERE ProjectID = '" + lblID.Text + "'", conn);
                    SqlDataAdapter daProject3 = new SqlDataAdapter(cmd3);
                    DataSet projectPoster = new DataSet();
                    conn.Open();
                    daProject3.Fill(projectPoster, "ProjectPoster");
                    conn.Close();
                    objProject.projectPoster = projectPoster.Tables["ProjectPoster"].Rows[0]["ProjectPoster"].ToString();
                }
                objProject.studentId = Convert.ToInt32(Session["StudentID"]);
                objProject.reflection = txtReflection.Text;
                objProject.updateProject();
                objProject.updateReflection();
                //Removal of members from the portfolio
                List<string> membersStill = new List<string>();
                List<string> membersRemoval = new List<string>();
                List<ListItem> listRemoval = new List<ListItem>();
                foreach(ListItem m in cblMembers.Items)
                {
                    if (m.Selected)
                        membersStill.Add(m.Value);
                    else
                    {
                        listRemoval.Add(m);
                        membersRemoval.Add(m.Value);
                    }   
                }
                for(int z = 0; z < membersRemoval.Count; z++)
                {
                    cblMembers.Items.Remove(listRemoval[z]);
                    string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
                    SqlConnection conn = new SqlConnection(sqlConn);
                    SqlCommand cmd4 = new SqlCommand("DELETE FROM ProjectMember WHERE ProjectID = '" + lblID.Text + "'AND StudentID = '" + membersRemoval[z]+  "'", conn);
                    conn.Open();
                    cmd4.ExecuteNonQuery();
                    conn.Close();
                }
                //Adding members
                if (txtAdd.Text.Count() > 0)
                {
                    List<string> newMembers = txtAdd.Text.Split(',').ToList();
                    foreach(string mem in newMembers)
                    {
                        string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
                        SqlConnection conn = new SqlConnection(sqlConn);
                        SqlCommand cmd = new SqlCommand("SELECT StudentID FROM Student WHERE NAME = '" + mem + "'", conn);
                        SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
                        DataSet StudentID = new DataSet();
                        conn.Open();
                        daStudent.Fill(StudentID, "StudentID");
                        conn.Close();
                        Project objproject2 = new Project();
                        objproject2.studentId = Convert.ToInt32(StudentID.Tables["StudentID"].Rows[0]["StudentID"]);
                        objproject2.role = "Member";
                        objproject2.projectId = Convert.ToInt32(lblID.Text);
                        objproject2.reflection = "";
                        objproject2.addMembersUpdate();
                        string newStudentID = StudentID.Tables["StudentID"].Rows[0]["StudentID"].ToString();
                        ListItem newMem = new ListItem(mem, newStudentID, true);
                        newMem.Selected = true;
                        cblMembers.Items.Add(newMem);
                    }
                    txtAdd.Text = "";
                  
                } 
            }
            lblCfm.Text = "Project Updated!";
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ReplyForm.aspx.cs" Inherits="Assignment1.ReplyForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 329px;
        }
        .auto-style2 {
            font-size: xx-large;
        }
        .auto-style3 {
            width: 329px;
            font-size: xx-large;
        }
        .auto-style4 {
            width: 329px;
            height: 36px;
        }
        .auto-style5 {
            height: 36px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <table class="w-100">
        <tr>
            <td class="auto-style3"><strong></strong></td>
            <td class="auto-style2"><strong>Reply</strong></td>
        </tr>
        <tr>
            <td class="auto-style1">Reply ID:</td>
            <td>
                <asp:Label ID="lblReply" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td class="auto-style1">Message ID:</td>
            <td>
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">Mentor Name:</td>
            <td class="auto-style5">
                <asp:Label ID="lblMentorName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Parent Name:</td>
            <td>
                <div class="ui form">
                    <div class="field">
                         <asp:DropDownList ID="ddlParentName" runat="server" CssClass="ui dropdown">
                </asp:DropDownList>
                    </div>
                </div>
               
                <asp:Label ID="lblPN" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">DateTime:</td>
            <td>
                <asp:Label ID="lblDT" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">Message:</td>
            <td>
                <div class="ui form">
                    <div class="field">
                        <textarea id="taMessage" cols="20" name="S1" rows="5" runat="server" maxlength="3000"></textarea>
                    </div>
                </div>
                </td>
        </tr>
        
    </table>
        
    <br />
    <asp:Button ID="btnReply" runat="server" Text="Reply" OnClick="btnReply_Click" CssClass="ui green button" />
</asp:Content>

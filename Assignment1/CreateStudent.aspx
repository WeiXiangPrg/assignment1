﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="CreateStudent.aspx.cs" Inherits="Assignment1.CreateStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style5 {
            font-size: x-large;
            font-weight: bold;
            height: 54px;
        }
        .auto-style7 {
            height: 53px;
        }
        .auto-style8 {
            font-size: x-large;
            font-weight: bold;
            height: 54px;
            width: 237px;
        }
        .auto-style9 {
            width: 237px;
            height: 67px;
        }
        .auto-style12 {
            height: 67px;
        }
        .auto-style13 {
            width: 237px;
            height: 53px;
        }
        .auto-style22 {
            width: 237px;
            height: 55px;
        }
        .auto-style25 {
            width: 237px;
            height: 51px;
        }
        .auto-style26 {
            height: 51px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellpadding="0" cellspacing="0" class="w-100">
        <tr>
            <td class="auto-style8">Create New Student</td>
            <td class="auto-style5">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style22">Username:</td>
            <td class="auto-style7">
                <div class="ui input">
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                </div>
                
        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Please specify a name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">Email:</td>
            <td class="auto-style7">
                <div class="ui input">
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </div>
                
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please specify a valid email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
&nbsp;<asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please specify a email" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">Course:</td>
            <td class="auto-style7">
                <div class="ui form">
                    <div class="field">
                        <asp:DropDownList ID="ddlCourse" runat="server" CssClass="ui dropdown">
                </asp:DropDownList>
                    </div>
                </div>
                
            </td>
        </tr>
        <tr>
            <td class="auto-style25">Mentor:</td>
            <td class="auto-style26">
                <div class="ui form">
                    <div class="field">
                        <asp:DropDownList ID="ddlMentor" runat="server" CssClass="ui dropdown">
                </asp:DropDownList>
                    </div>
                </div>
                
                <asp:RequiredFieldValidator ID="rfvsMentor" runat="server" ControlToValidate="ddlMentor" ErrorMessage="Select a mentor" InitialValue="--Select--"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">Upload Picture:</td>
            <td class="auto-style7">
                <asp:FileUpload ID="upPhoto" runat="server" accept=".jpg" />
            </td>
        </tr>
        <tr>
            <td class="auto-style13">StudentID:</td>
            <td class="auto-style7">
                <asp:Label ID="lblStudent" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style13">&nbsp;</td>
            <td class="auto-style7">
                <asp:Image ID="imgPhoto" runat="server" Height="100px" />
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style12">
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" CssClass="ui primary button" />
                <asp:CustomValidator ID="cuvSEmail" runat="server" Display="Dynamic" ErrorMessage="E-mail address has been used." ForeColor="Red" OnServerValidate="cuvEmail_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">&nbsp;</td>
            <td class="auto-style12">
                <asp:Label ID="lblValue" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class PostSuggestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                DisplayStudentName();
            }
        }

        protected void btnPost_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                
                if(ddlStudentName.SelectedIndex != 0)
                {
                    DateTime dTime = DateTime.Now;
                    lblDT.Text = dTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    Students objStudent = new Students();

                    objStudent.suggestionid = Convert.ToInt32(Request.QueryString["SuggestionID"]);
                    objStudent.mentorid = Convert.ToInt32(Session["MentorID"]);
                    lblMentorName.Text = Session["Name"].ToString();
                    objStudent.studentid = Convert.ToInt32(ddlStudentName.SelectedValue);
                    objStudent.dt = Convert.ToDateTime(lblDT.Text);
                    objStudent.desc = taMessage.Value;
                    objStudent.status = "N";
                    lblStatus.Text = "N";

                    int id = objStudent.suggest();

                    lblSuggestion.Text = id.ToString();

                    objStudent.PostStatus("N");
                    lblStudentName.Visible = false;
                }
                else if(ddlStudentName.SelectedIndex == 0)
                {
                    lblStudentName.Text = "Please select valid student name";
                    lblStudentName.Visible = true;
                }
                
                
            }
        }

        private void DisplayStudentName()
        {

            //Read connection string "NPPortfolioConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object. supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);

            //Declare and instantiate DataAdapter object
            SqlDataAdapter daSkill = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            conn.Open();
            DataSet result = new DataSet();

            //Use DataApadter to fetch data to a table "StudentIDDetails" in DataSet.
            daSkill.Fill(result, "StudentIDDetails");
            conn.Close();

            //Specify GridView to get data from table "StudentIDDetails"
            //in DataSet "result"
            ddlStudentName.DataSource = result.Tables["StudentIDDetails"];

            //Specify the Value property of dropdownlist
            ddlStudentName.DataValueField = "StudentID";
            ddlStudentName.DataTextField = "Name";
            //Display the list of data in GridView
            ddlStudentName.DataBind();
            
            //Insert prompt for the DropDownList at the first position of the list
            ddlStudentName.Items.Insert(0, "--Select--");
        }
    }
}
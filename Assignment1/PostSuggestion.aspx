﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="PostSuggestion.aspx.cs" Inherits="Assignment1.PostSuggestion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            font-weight: bold;
            font-size: xx-large;
        }
        .auto-style5 {
            height: 33px;
        }
        .auto-style6 {
            height: 32px;
        }
        .auto-style7 {
            width: 136px;
            height: 71px;
        }
        .auto-style8 {
            width: 166px;
        }
        .auto-style9 {
            height: 33px;
            width: 166px;
        }
        .auto-style10 {
            height: 32px;
            width: 166px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style8"><strong></strong></td>
            <td class="auto-style2"><strong>Posts</strong></td>
        </tr>
        <tr>
            <td class="auto-style9">Suggestion ID:</td>
            <td class="auto-style5">
                <asp:Label ID="lblSuggestion" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style10">Mentor Name:</td>
            <td class="auto-style6">
                <asp:Label ID="lblMentorName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">Student Name:</td>
            <td>
                <asp:DropDownList ID="ddlStudentName" runat="server">
                </asp:DropDownList>
                <asp:Label ID="lblStudentName" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">Status:</td>
            <td>
                 <asp:Label ID="lblStatus" runat="server" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">Message:</td>
            <td>
                  <div class="ui form">
                    <div class="field">
                   
                        <textarea id="taMessage" name="S1" runat="server" maxlength="3000"  class="auto-style7"></textarea></td>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="auto-style8">DateCreated:</td>
            <td>
                <asp:Label ID="lblDT" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <br />
        <asp:Button ID="btnPost" runat="server" CssClass="ui teal button" Text="Post" Width="126px" OnClick="btnPost_Click" />
        <br />
</asp:Content>

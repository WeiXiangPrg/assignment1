﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="CreateMentor.aspx.cs" Inherits="Assignment1.CreateMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 223px;
            height: 52px;
        }
        .auto-style3 {
            height: 52px;
        }
        .auto-style4 {
            height: 53px;
        }
        .auto-style5 {
           font-size: x-large;
            font-weight: bold;
            width: 223px;
        }
        .auto-style8 {
            width: 100%;
            height: 255px;
            margin-bottom: 23px;
        }
        .auto-style9 {
            width: 223px;
            height: 53px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellpadding="0" cellspacing="0" class="auto-style8">
        <tr>
            <td class="auto-style5">Create New Mentor</td>
        </tr>
        <tr>
            <td class="auto-style9">Username:</td>
            <td class="auto-style4">
                <div class="ui input">
                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                </div>
                
        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="Please specify a name" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style9">Email:</td>
            <td class="auto-style4">
                <div class="ui input">
                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                </div>
                
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please specify a valid email" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
&nbsp;<asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please specify a email" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">MentorID:</td>
            <td class="auto-style3">
                <asp:Label ID="lblID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" CssClass="ui primary button" />
                <asp:CustomValidator ID="cuvEmail" runat="server" Display="Dynamic" ErrorMessage="E-mail address has been used." ForeColor="Red" OnServerValidate="cuvEmail_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">
                <asp:Label ID="lblValue" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

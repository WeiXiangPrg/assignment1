﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="CreateSkills.aspx.cs" Inherits="Assignment1.CreateSkills" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
            font-weight: bold;
            width: 296px;
        }
        .auto-style2 {
            width: 296px;
            height: 51px;
        }
        .auto-style3 {
            height: 51px;
        }
        .auto-style4 {
            width: 296px;
            height: 58px;
        }
        .auto-style5 {
            height: 58px;
        }
         .auto-style6 {
            margin-left: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >

    <table class="w-100">
        <tr>
            <td class="auto-style1">Create Choices of Skill Set</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2">Skills set:</td>
            <td>
                <div class="ui input">
                    <asp:TextBox ID="txtSkillset" runat="server"></asp:TextBox>
                </div>
                
        <asp:RequiredFieldValidator ID="rfvSkill" runat="server" ControlToValidate="txtSkillset" Display="Dynamic" ErrorMessage="Please specify a Skill" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Label ID="lblSkills" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">
                <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" CssClass="ui primary button" />
                <asp:CustomValidator ID="cuvSkillSet" runat="server" Display="Dynamic" ErrorMessage="SkillSet already exist" ForeColor="Red" OnServerValidate="cuvSkillSet_ServerValidate"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4"></td>
            <td class="auto-style5">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>

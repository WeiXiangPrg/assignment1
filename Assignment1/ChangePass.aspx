﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ChangePass.aspx.cs" Inherits="Assignment1.ChangePass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
        }
        .auto-style2 {
            width: 762px;
        }
        .auto-style3 {
            width: 765px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <p class="auto-style1" style="text-align:center;">
        <strong>Change Password</strong></p>
    <table class="w-100" style="text-align:center;"">
        <tr>
            <td class="auto-style2" style="text-align:right;">Email Address:</td>&nbsp
            <td class="auto-style3" style="text-align:left;">
                <asp:Label ID="lblEmail" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" style="text-align:right;">New Password:</td>&nbsp
            <td class="auto-style3" style="text-align:left;">
                <div class="ui input">
                <asp:TextBox ID="txtNewPass" runat="server" CssClass="ui input"></asp:TextBox>
                    </div>
                <asp:RequiredFieldValidator ID="rfvNewPass" runat="server" ControlToValidate="txtNewPass" Display="Dynamic" ErrorMessage="You are required to fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revNewPass" runat="server" ControlToValidate="txtNewPass" Display="Dynamic" ErrorMessage="Please key in at least 8 characters and 1 digit(0-9)" ForeColor="Red" ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" style="text-align:right;">Confirm New Password:</td>&nbsp
            <td class="auto-style3" style="text-align:left;">
                <div class="ui input">
                <asp:TextBox ID="txtCfmNewPass" runat="server" CssClass="ui input"></asp:TextBox>
                    </div>
                <asp:RequiredFieldValidator ID="rfvCfmNewPass" runat="server" ControlToValidate="txtCfmNewPass" Display="Dynamic" ErrorMessage="You are required to fill in" ForeColor="Red"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvCfmNewPass" runat="server" ControlToCompare="txtNewPass" ControlToValidate="txtCfmNewPass" Display="Dynamic" ErrorMessage="Does not match" ForeColor="Red"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2" style="text-align:center;">&nbsp;</td>
            <td class="auto-style3" style="text-align:left;">

                <asp:Label ID="lblMessage" runat="server"></asp:Label>

                <br />
                <asp:Button ID="btnChange" runat="server" Text="Change" OnClick="btnChange_Click" CssClass="ui positive button" />&nbsp&nbsp

                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CauseValidation="False" CssClass="ui negative button"/>

            </td>
        </tr>
    </table>
</asp:Content>

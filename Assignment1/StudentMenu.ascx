﻿    <%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StudentMenu.ascx.cs" Inherits="Assignment1.StudentMenu" %>
<!-- A grey navbar that expands horizontally at medium device -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- The brand(or icon) of the navbar -->
    <a class="navbar-brand" href="Student.aspx"
        style="font-size:32px; font-weight:bold; color:#808080;">
        ABC Polytechnic
    </a>
    <!-- Toggle/collpsible Button, also known as hamburger button -->
   <button class="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#ABCNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

                
    <!--Links in the navbar, displayed as drop-down list when collasped -->
    <div class="collapse navbar-collapse" id="ABCNavbar">
        <!-- Links that are aligned to the left, 
            mr-auto: right margin auto-adjudted -->

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="Profile.aspx">View Profile</a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="Portfolio.aspx">View Portfolios</a>
             </li>
            <li class="nav-item">
                <a class="nav-link" href="CreatePortfolio.aspx">Create Portfolios</a>
             </li>
             <li class="nav-item">
                <a class="nav-link" href="Suggestions.aspx"> Suggestions </a>

                </li>

          
        </ul>
        <!-- Links that are aligned to the right, ml-auto: left margin auto-adjusted -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- A Web form control button for logging our user -->
                <i class="power off icon"></i>
                   <asp:Button ID="btnLogOut" runat="server" Text="Log Out"  CausesValidation="false"
                CssClass="negative ui button" OnClick="btnLogOut_Click"/>
            </li>
         
        </ul>
    </div>
</nav>
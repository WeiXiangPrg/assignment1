﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class UpdateProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmailAddr"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                lblEmail.Text = Session["EmailAddr"].ToString();
                txtDesc.Text = Session["Description"].ToString();
                txtLink.Text = Session["Link"].ToString();
                txtAch.Text = Session["Achievement"].ToString();
                List<string> skillList = Session["SkillNameList"].ToString().Split(',').ToList();
                List<string> skillIDs = Session["SkillIDList"].ToString().Split(',').ToList();
                List<string> currentSkills = Session["SkillSetName"].ToString().Split(',').ToList();
                for (int x = 0; x < skillList.Count; x++)
                {
                    int count = 0;
                    ListItem sk = new ListItem(skillList[x], skillIDs[x], true);
                    foreach (string skill in currentSkills)
                    {
                        if (skillList[x] == skill)
                            count = 1;
                    }
                    if (count > 0)
                        sk.Selected = true;
                    else
                        sk.Selected = false;
                    cblSkills.Items.Add(sk);

                }


            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            lblCfm.Text = "";
            //Update of the profile of the student
            List<string> selectedSkillID = cblSkills.Items.Cast<ListItem>()
                .Where(li => li.Selected).Select(li => li.Value).ToList();
            List<string> selectedSkillNames = cblSkills.Items.Cast<ListItem>()
              .Where(li => li.Selected).Select(li => li.Text).ToList();
            //Validates that the user must have at least one skillset
            if (selectedSkillID.Count == 0)
            {
                lblError.Text = "Update Unsuccessful!Please Select at least one skill!";
                return;
            }
            Students objStudent = new Students();
            //Validates Same Emails are not allowed
            if (objStudent.isEmailExist(txtEmail.Text) == true)
            {
                lblError.Text = "Update Unsuccessful! Email Already Exists!";
                return;
            }
            lblError.Text = "";
            objStudent.studentid = Convert.ToInt32(Session["StudentID"]);
            if (txtEmail.Text.Count() > 1 && txtEmail.Text != lblEmail.Text)
            {
                objStudent.email = txtEmail.Text;
                Session["EmailAddr"] = txtEmail.Text;
            }
            else
            {
                objStudent.email = lblEmail.Text;
                Session["EmailAddr"] = lblEmail.Text;
            }

            objStudent.description = txtDesc.Text;
            objStudent.achievement = txtAch.Text;
            objStudent.link = txtLink.Text;
            objStudent.delSkills();
            for (int s = 0; s < selectedSkillID.Count; s++)
            {
                objStudent.skillsetid = Convert.ToInt32(selectedSkillID[s]);
                objStudent.updateSkills();
            }
            string uploadedFile = "";
            if (upPhoto.HasFile == true)
            {
                string savePath;

                //Find the filename extension of the file to be uploaded.
                string fileExt = Path.GetExtension(upPhoto.FileName);
                uploadedFile = upPhoto.FileName;

                //MapPath - to fins the complete path to the images folder in server
                savePath = MapPath("~/Students/" + uploadedFile);
                objStudent.photo = upPhoto.FileName;
                Session["Photo"] = upPhoto.FileName;
                try
                {
                    upPhoto.SaveAs(savePath); //Upload the file to server
                    lblMsg.Text = "File uploaded successfully.";
                    imgPhoto.ImageUrl = "~/Students/" + uploadedFile;

                }
                catch (IOException)
                {
                    //File IO error, could due to access rights denied
                    lblMsg.Text = "File uploading fail!";

                }
                catch (Exception ex) // other type of error
                {
                    lblMsg.Text = ex.Message;
                }
            }
            else
            {
                objStudent.photo = Session["Photo"].ToString();
            }
            //Updating the Session details of the users
            Session["SkillSetName"] = string.Join(",", selectedSkillNames.ToArray());
            Session["Achievement"] = txtAch.Text;
            Session["Link"] =  txtLink.Text;
            Session["Description"] = txtDesc.Text;
            objStudent.updateProfile();
            lblCfm.Text = "Profile Successfully Updated!";

        }

    }
}
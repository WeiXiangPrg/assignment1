﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class CreateMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //Create a new object from the Mentor Class
                Mentors objMentor = new Mentors();

                //Pass data to the properties of the Staff object,
                //do the necessary data type conversion if need to.
                objMentor.name = txtName.Text;
                objMentor.email = txtEmail.Text;



                //Call the add method to insert the mentor record to database.
                int id = objMentor.add();
                lblID.Text = id.ToString();

                lblValue.Text = "Successfully added Mentor!";
            }
        }

        protected void cuvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid) //other client-side validation passed
            {
                Mentors objMentor = new Mentors();

                if (objMentor.isEmailExist(txtEmail.Text) == true)
                    args.IsValid = false; //Raise error
                else
                    args.IsValid = true; //No error
            }
        }
    }
}
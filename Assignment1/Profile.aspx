﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="Assignment1.Profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-left: 40px;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-size: x-large;
            font-weight: bold;
        }
        .auto-style3 {
            font-family: Arial;
            font-size: medium;
            display: block;
        }
        .auto-style4 {
            font-family: Arial;
            font-size: medium;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContentPlaceHolder" runat="server">
    <p class="auto-style2">
        My Profile
    </p>
    <p>
        <asp:Image ID="imgProfile" CssClass="ui small circular image" runat="server"  />
    </p>

    <p>
        <div class ="ui teal message">
            <div class="header" style="font-family: Arial; font-size: medium"> My Personal Information</div>
            <p></p>
             <strong>Name:&nbsp;</strong>
        <asp:Label ID="lblName" runat="server"></asp:Label>
    <p>
        <strong>StudentID:</strong>
        <asp:Label ID="lblID" runat="server"></asp:Label>
    </p>
    <p>
        <strong>Email: </strong>&nbsp;<asp:Label ID="lblEmail" runat="server"></asp:Label>
    </p>
    <p>
        <strong>Course:</strong>
        <asp:Label ID="lblCourse" runat="server"></asp:Label>
    </p>
    </div>
            <div class="ui teal message">
            <div class ="header" style="font-family: Arial; font-size: medium">
                Description
            </div>
            <p>
                <asp:Label ID="lblDesc" runat="server"></asp:Label>
            </p>
        </div>
    <div class="ui teal message">
        <div class="header">
                <span class="auto-style3">Achievements</span>
             </div>
            <p>
        <asp:Label ID="lblAch" runat="server"></asp:Label>
            </p>
       
    </div>

        <div class="ui teal message">
        <div class="header" style="font-family: Arial; font-size: medium">
            My Personal Link
             </div>
        <p>
        
        <asp:Label ID="lblLink" runat="server"></asp:Label>
            </p>
       
    </div>

    <p>
        <div class="ui teal message">
            <div class ="header">
                <span class="auto-style4">Skills</span>
            </div>
            <p>
                 <asp:Label ID="lblSkills" runat="server"></asp:Label>
            </p>
        </div>
       
    &nbsp;&nbsp;&nbsp;
    </p>
    <p>
        <asp:LinkButton ID="Update"  runat="server" CssClass="ui primary button" OnClick="Update_Click">Update Profile</asp:LinkButton>
    </p>
    <p>
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlaceHolder" runat="server">
    <p class="auto-style1">
        &nbsp;</p>
</asp:Content>

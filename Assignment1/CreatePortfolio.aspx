﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="CreatePortfolio.aspx.cs" Inherits="Assignment1.CreatePortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: Arial;
            font-size: x-large;
            font-weight: bold;
        }
        .auto-style2 {
            color: #CC0000;
        }
        .auto-style4 {
            font-family: Arial;
            font-size: 0.928571em;
        }
        .auto-style5 {
            font-family: Arial;
            font-weight: bold;
            font-size: 0.928571em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContentPlaceHolder" runat="server">
    <p class="auto-style1">
        Create Portfolio</p>
    <p class="auto-style5">
        Title
        <div class="ui input"><asp:TextBox ID="txtTitle" runat="server" ></asp:TextBox></div>
        
        </br>
        <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle" Display="Dynamic" ErrorMessage="Please enter a Title" ForeColor="Red"></asp:RequiredFieldValidator>
    </p>
    <p class="auto-style5">
        Leader Name
        <div class="ui input"><asp:TextBox ID="txtLeader" runat="server" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
         <asp:RequiredFieldValidator ID="rfvLeader" runat="server" ControlToValidate="txtLeader" ErrorMessage="Please enter leader name(Your name)" ForeColor="Red">*</asp:RequiredFieldValidator>
        </div>
         </br>
       
    </p>
    <p>
        <span class="auto-style5">Members</span>
        <div class="ui input"><asp:TextBox ID="txtMembers" runat="server" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvMember" runat="server" ControlToValidate="txtMembers" Display="Dynamic" ErrorMessage="Please enter member names" ForeColor="Red">*</asp:RequiredFieldValidator>
        </div>
        </br>
        <span class="auto-style2">*(Enter Members Full Name)</span></p>
    <p>
        <div class ="ui form">
        <div class ="field">
            <label>Project Description</label>
        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine"></asp:TextBox>
            </div>
            </div>
    </p>
    <p class="auto-style4">
        <strong>Project URL
        </strong>
        <div class="ui input">
            
             <asp:TextBox ID="txtURL" runat="server" onkeydown = "return (event.keyCode!=13);" ></asp:TextBox>
        </div>
    </p>
    <p>
        <span class="auto-style5">Upload Poster Image :</span>
        <asp:FileUpload ID="upPoster" runat="server" />
        <asp:RegularExpressionValidator ID="revUpload" runat="server" ControlToValidate="upPoster" Display="Dynamic" ErrorMessage="Only Images formats allowed." ForeColor="Red" ValidationExpression="(.*png$)|(.*jpg$)|(.*jpeg$)"></asp:RegularExpressionValidator>
    </p>
    <p>
        <asp:Image ID="imgPoster" runat="server" Height="100px" />
    </p>
    <p>
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
    </p>
    <p>
        <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" CssClass="ui primary button" />
        <asp:Label ID="lblCfm" runat="server"></asp:Label>
    </p>
    <p class="auto-style1">
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlaceHolder" runat="server">
</asp:Content>

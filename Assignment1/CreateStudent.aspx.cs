﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class CreateStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Page.IsPostBack == false)
            {
                ddlCourse.Items.Add("IT");
                ddlCourse.Items.Add("FI");
                imgPhoto.ImageUrl = "";
                displayMentorDropDownList();
            }

        }


        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string uploadedFile = "";
                if (upPhoto.HasFile == true)
                {
                    string savePath;

                    //Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(upPhoto.FileName);

                    //Rename the uploaded file with the staff's name 
                    uploadedFile = txtName.Text + fileExt;

                    //MapPath - to fins the complete path to the images folder in server
                    savePath = MapPath(uploadedFile);

                    try
                    {
                        upPhoto.SaveAs(savePath); //Upload the file to server
                        lblMsg.Text = "Picture uploaded successfully.";
                        imgPhoto.ImageUrl = uploadedFile;

                    }
                    catch (IOException)
                    {
                        //File IO error, could due to access rights denied
                        lblMsg.Text = "Picture uploading fail!";

                    }

                    catch (Exception ex) // other type of error
                    {
                        lblMsg.Text = ex.Message;
                    }

                }
                //Create a new object from the Student Class
                Students objStudent = new Students();

                //Pass data to the properties of the Student object,
                //do the necessary data type conversion if need to.
                objStudent.name = txtName.Text;
                objStudent.course = ddlCourse.SelectedValue;
                objStudent.email = txtEmail.Text;
                objStudent.photo = imgPhoto.ImageUrl;
                objStudent.mentorid = Convert.ToInt32(ddlMentor.SelectedValue);

                //Call the add method to insert the Student record to database.
                int id = objStudent.add();

                lblStudent.Text = id.ToString();


                lblValue.Text = "Successfully added Student!";
            }
        }
        protected void cuvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid) //other client-side validation passed
            {
                Students objStudent = new Students();

                if (objStudent.isEmailExist(txtEmail.Text) == true)
                    args.IsValid = false; //Raise error
                else
                    args.IsValid = true; //No error
            }
        }
        private void displayMentorDropDownList()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SELECT SQL statement which retrieves all attributes of a branch, and a connection object to open the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Mentor", conn);

            //Create a DataAdapter object
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);

            //Create DataSet object
            DataSet result = new DataSet();

            //Open a database connection.
            conn.Open();

            //Use DataAdapter to fetch data to a table "BranchDetails" in DataSet.
            daMentor.Fill(result, "MentorDetails");

            //Close database connection
            conn.Close();

            //Specify the dropdown list to get data from the dataset
            ddlMentor.DataSource = result.Tables["MentorDetails"];

            //Specify the Value property of dropdownlist
            ddlMentor.DataTextField = "Name";
            ddlMentor.DataValueField = "MentorID";

            // Load Branch information to the drop-down list
            ddlMentor.DataBind();

            //Insert prompt for the DropDownList at the first position of the list
            ddlMentor.Items.Insert(0, "--Select--");
        }
    }
}


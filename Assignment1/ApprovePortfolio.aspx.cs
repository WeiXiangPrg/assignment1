﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class ViewE_Portfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                StudentList();
                StudentPortfolioList();
                StudentProjectList();
            }
        }
        
        private void StudentList()
        {

            //Read connection string "NPPortfolioConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object. supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);

            //Declare and instantiate DataAdapter object
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            conn.Open();
            DataSet result = new DataSet();

            //Use DataAdapter to fetch data to a table "StudentDetails" in DataSet.
            daStudent.Fill(result, "StudentDetails");
            conn.Close();

            //Specify GridView to get data from table "StudentDetails"
            //in DataSet "result"
            gvStudent.DataSource = result.Tables["StudentDetails"];

            //Display the list of data in GridView
            gvStudent.DataBind();
        }

        private void StudentPortfolioList()
        {
            //Read connection string "NPPortfolioConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Project", conn);

            SqlDataAdapter daPort = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operations made.
            conn.Open();

            //Use DateAdapter to fetch data to a table "PortfolioDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation.
            daPort.Fill(result, "PortfolioDetails");

            //A connection should always be closed, whether error occurs or not.
            conn.Close();

            //Specify GridView to get data from table "PortfolioDetails"
            //in DataSet "result"
            gvProject.DataSource = result.Tables["PortfolioDetails"];

            //Display the list of data in GridView
            gvProject.DataBind();
        }

        private void StudentProjectList()
        {
            //Read connection string "NPPortfolioConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT * FROM ProjectMember", conn);

            SqlDataAdapter daProj = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operations made.
            conn.Open();

            //Use DateAdapter to fetch data to a table "ProjectDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation.
            daProj.Fill(result, "ProjectDetails");

            //A connection should always be closed, whether error occurs or not.
            conn.Close();

            //Specify GridView to get data from table "ProjectDetails"
            //in DataSet "result"
            gvMember.DataSource = result.Tables["ProjectDetails"];

            //Display the list of data in GridView
            gvMember.DataBind();
        }

        protected void gvStudent_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
            if (e.CommandName == "Disapprove")
            {
                //Retrieve the row index stored in the CommandArgument property
                int i = Convert.ToInt32(e.CommandArgument);

                Students objStudent = new Students();

                objStudent.studentid = Convert.ToInt32(gvStudent.Rows[i].Cells[0].Text);
                

                int errorCode = objStudent.Disapprove("N");
                lblMessage.Text = "Updated Successfully";

            }
            else if (e.CommandName == "Approve")
            {
                //Retrieve the row index stored in the CommandArgument property
                int i = Convert.ToInt32(e.CommandArgument);

                Students objStudent = new Students();

                objStudent.studentid = Convert.ToInt32(gvStudent.Rows[i].Cells[0].Text);              

                int errorCode = objStudent.Approve("Y");
                lblMessage.Text = "Updated Successfully";
            }
            StudentList();
        }
    }
}
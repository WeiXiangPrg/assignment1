﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public class SystemAdmin
    {
        public int requestId { get; set; }
        public string status { get; set; }


        public int updateStatus(string status)
        {
            //Read connection string "NPBookConnectionString" from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection objectt with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlConnection object,  supply it with a SQL statement 
            SqlCommand cmd = new SqlCommand
                ("Update ViewingRequest Set Status=@status Where ViewingRequestID=@selectedViewingRequestID",
                conn);

            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@selectedViewingRequestID", requestId);

            //Open a database connection
            conn.Open();
            //Execute NonQuery is used for Update SQL statement 
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs
            return 0;

        }
    }
}
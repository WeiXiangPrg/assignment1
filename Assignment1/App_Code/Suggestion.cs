﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Suggestion
    {
        public int suggestionID { get; set; }
        public string status { get; set; }
       

       public int update(int suggestionID,string status)
        {
            //Read connection string "NPBookConnectionString" from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SQL Statement
            SqlCommand cmd = new SqlCommand
                ("UPDATE Suggestion SET Status=@newStatus WHERE SuggestionID=@selectedSuggestionID", conn);

            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd.Parameters.AddWithValue("@newStatus", status);
            cmd.Parameters.AddWithValue("@selectedSuggestionID", suggestionID);

            //Open a Database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE SQL statement/
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs.
            return 0;
        }
    }
}
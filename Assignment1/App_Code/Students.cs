﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public class Students
    {
        public int studentid { get; set; }
        public string name { get; set; }
        public string course { get; set; }
        public string photo { get; set; }
        public string description { get; set; }
        public string externallink { get; set; }
        public string password { get; set; }
        public string achievement { get; set; }
        public string email { get; set; }
        public string status { get; set; }
        public int mentorid { get; set; }
        public string skill { get; set; }
        public string link { get; set; }
        public int skillsetid { get; set; }
        public int suggestionid { get; set; }
        public string desc { get; set; }
        public DateTime dt { get; set; }


        public int add()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings
                             ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlConnection object, supply it with an INSERT SQL statement 
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                             ("INSERT INTO Student (Name, Course, Photo, " +
                             " EmailAddr, MentorID) " +
                             "OUTPUT INSERTED.StudentID " +
                             "VALUES(@name, @course, @photo," +
                             " @email, @mentorid)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@course", course);
            cmd.Parameters.AddWithValue("@photo", photo);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@mentorid", mentorid);




            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            int id = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();

            //Return id when no error occurs.
            return id;
        }

        public int addskills()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings
                             ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlConnection object, supply it with an INSERT SQL statement 
            //which will return the auto-generated StaffID after insertion,
            //and the connection object for connecting to the database.
            SqlCommand cmd = new SqlCommand
                             ("INSERT INTO SkillSet (SkillSetName)" +
                             "OUTPUT INSERTED.SkillSetID " +
                             "VALUES(@skillsetname)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@skillsetname", skill);

            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            int id = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();

            //Return id when no error occurs.
            return id;
        }

        public int getSDetails()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);


            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            //Open a database connection.
            conn.Open();

            //Use DateAdapter to fetch data to a table"StaffDetails" in DataSet.
            daStudent.Fill(result, "StudentDetails");

            //Close database connection
            conn.Close();

            if (result.Tables["StudentDetails"].Rows.Count > 0)
            {
                //Fill Staff object with values from the DataSet
                DataTable table = result.Tables["StudentDetails"];
                
                if (!DBNull.Value.Equals(table.Rows[0]["StudentID"]))
                {
                    studentid = Convert.ToInt32(table.Rows[0]["StudentID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Name"]))
                {
                    name = table.Rows[0]["Name"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Course"]))
                {
                    course = table.Rows[0]["Course"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Description"]))
                {
                    description = table.Rows[0]["Description"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Achievement"]))
                {
                    achievement = table.Rows[0]["Achievement"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["EmailAddr"]))
                {
                    email = table.Rows[0]["EmailAddr"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["Status"]))
                {
                    status = table.Rows[0]["Status"].ToString();
                }
                if (!DBNull.Value.Equals(table.Rows[0]["MentorID"]))
                {
                    mentorid = Convert.ToInt32(table.Rows[0]["MentorID"]);
                }
                //Return 0 when no error occurs.
                return 0;
            }
            else
            {
                return -2;
            }
        }

        public int getSkillDetails()
        {
 
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);


            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet", conn);


            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            //Open a database connection.
            conn.Open();

            //Use DateAdapter to fetch data to a table"StaffDetails" in DataSet.
            daStudent.Fill(result, "SkillDetails");

            //Close database connection
            conn.Close();

            if (result.Tables["SkillDetails"].Rows.Count > 0)
            {
                //Fill Staff object with values from the DataSet
                DataTable table = result.Tables["SkillDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetID"]))
                {
                    skillsetid = Convert.ToInt32(table.Rows[0]["SkillSetID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetName"]))
                {
                    skill = table.Rows[0]["SkillSetName"].ToString();
                }
                
                //Return 0 when no error occurs.
                return 0;
            }
            else
            {
                return -2;
            }
        }

        public int getSkillSetDetails()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SELECT SQL
            //statement which retrieves all attributes of a staff record.
            SqlCommand cmd = new SqlCommand("SELECT * FROM StudentSkillSet", conn);


            //Instantiate a DataAdapter object, pass the SqlCommand
            //object created as parameter.
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            //Open a database connection.
            conn.Open();

            //Use DateAdapter to fetch data to a table"StaffDetails" in DataSet.
            daStudent.Fill(result, "SkillSetDetails");

            //Close database connection
            conn.Close();

            if (result.Tables["SkillSetDetails"].Rows.Count > 0)
            {
                //Fill Staff object with values from the DataSet
                DataTable table = result.Tables["SkillSetDetails"];

                if (!DBNull.Value.Equals(table.Rows[0]["StudentID"]))
                {
                    studentid = Convert.ToInt32(table.Rows[0]["StudentID"]);
                }
                if (!DBNull.Value.Equals(table.Rows[0]["SkillSetID"]))
                {
                    skillsetid = Convert.ToInt32(table.Rows[0]["SkillSetID"]);
                }

                //Return 0 when no error occurs.
                return 0;
            }
            else
            {
                return -2;
            }
        }

        public int Approve(string status)
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status = @status" + " WHERE StudentID = @selectedstudentid", conn);

            //Instantiate a DateAdapter object and pass the SqlCommand object
            //created as parameter.
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@selectedstudentid", studentid);
            //Open a database connection
            conn.Open();
            //Execute NonQuery is used for Update SQL statement 
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs
            return 0;
        }

        public int Disapprove(string status)
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status = @status" + " WHERE StudentID = @selectedstudentid", conn);

            //Instantiate a DateAdapter object and pass the SqlCommand object
            //created as parameter.
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@selectedstudentid", studentid);
            //Open a database connection
            conn.Open();
            //Execute NonQuery is used for Update SQL statement 
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs
            return 0;
        }

        public int PostStatus(string status)
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status = @status" + " WHERE StudentID = @selectedstudentid", conn);

            //Instantiate a DateAdapter object and pass the SqlCommand object
            //created as parameter.
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@selectedstudentid", studentid);
            //Open a database connection
            conn.Open();
            //Execute NonQuery is used for Update SQL statement 
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs
            return 0;
        }
        /*public int updatePost(int studentid, string status)
        {
            //Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("UPDATE Student SET Status = N" + " WHERE StudentID = @selectedstudentid", conn);

            //Instantiate a DateAdapter object and pass the SqlCommand object
            //created as parameter.
            cmd.Parameters.AddWithValue("N", status);
            cmd.Parameters.AddWithValue("@selectedstudentid", studentid);
            //A connection must be opened before any operations made.
            conn.Open();

            //Use DateAdapter to fetch data to a table "StaffDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation.
            int count = cmd.ExecuteNonQuery();

            //A connection should always be closed, whether error occurs or not.
            conn.Close();


            if (count > 0)
            {
                return 0;
            }
            else
            {
                return -2;
            }
        }*/
        public int suggest()
        {
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("INSERT INTO Suggestion (MentorID, StudentID, Description, Status, DateCreated) " + "OUTPUT INSERTED.SuggestionID " + "VALUES (@mentorID, @studentID,  @Text, @status, @dateCreated)", conn);

            cmd.Parameters.AddWithValue("@mentorID", mentorid);
            cmd.Parameters.AddWithValue("@studentID", studentid);
            cmd.Parameters.AddWithValue("@Text", desc);
            cmd.Parameters.AddWithValue("@status", status);
            cmd.Parameters.AddWithValue("@dateCreated", dt);
            conn.Open();

            int id = (int)cmd.ExecuteScalar();

            conn.Close();
            return id;
        }
        public bool isEmailExist(string email)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                             ["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                            ("SELECT * FROM Student WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            //use DataAdapter to fetch data to a table "EmailDetails" in Dataset.
            daEmail.Fill(result, "EmailDetails");
            conn.Close();

            if (result.Tables["EmailDetails"].Rows.Count > 0)
                return true; //the email given exists
            else
                return false; //the email given does not exist
        }

        public bool isSkillsExist(string skill)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                             ["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                            ("SELECT * FROM SkillSet WHERE SkillSetName=@selectedskillsetname", conn);
            cmd.Parameters.AddWithValue("@selectedskillsetname", skill);

            SqlDataAdapter daSkill = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            //use DataAdapter to fetch data to a table "EmailDetails" in Dataset.
            daSkill.Fill(result, "SkillSetName");
            conn.Close();

            if (result.Tables["SkillSetName"].Rows.Count > 0)
                return true; //the email given exists
            else
                return false; //the email given does not exist
        }
        public int updateProfile()
        {
            //Read connection string "NPBookConnectionString" from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SQL Statement
            SqlCommand cmd = new SqlCommand
                ("UPDATE Student SET EmailAddr=@newEmailAddr,Photo=@newPhoto,Description=@newDescription" +
                ",Achievement=@newAchievement,ExternalLink=@newExternalLink "  +
                " WHERE StudentId=@selectedStudentID", conn);

            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd.Parameters.AddWithValue("@newPhoto", photo);
            cmd.Parameters.AddWithValue("@newDescription", description);
            cmd.Parameters.AddWithValue("@newAchievement", achievement);
            cmd.Parameters.AddWithValue("@newExternalLink", link);
            cmd.Parameters.AddWithValue("@newEmailAddr", email);
            cmd.Parameters.AddWithValue("@selectedStudentID", studentid);
           
            //Open a Database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE SQL statement/
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs.
            return 0;
        }
        public int delSkills() // To update skills
        {
            string strConn = ConfigurationManager.ConnectionStrings
                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SQL Statement
            SqlCommand cmd2 = new SqlCommand
                ("DELETE FROM StudentSkillSet WHERE StudentID = @StudentID", conn);
            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd2.Parameters.AddWithValue("@StudentID", studentid);

            //Open a Database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE SQL statement/
            cmd2.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs.
            return 0;
        }
        public int updateSkills()
        {
            string strConn = ConfigurationManager.ConnectionStrings
                 ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SQL Statement
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO StudentSkillSet(StudentID,SkillSetID) VALUES(@StudentID,@SkillSetID)", conn);
            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd.Parameters.AddWithValue("@SkillSetID", skillsetid);
            cmd.Parameters.AddWithValue("@StudentID", studentid);

            //Open a Database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE SQL statement/
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs.
            return 0;
        }


    }
}
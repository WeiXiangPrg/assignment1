﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public class Mentors
    {
        public int mentorid { get; set; }
        public int messageid { get; set; }
        public int parentid { get; set; }
        public string Mtext { get; set; }
        public DateTime dt { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public int update()
        {
            //Read connection string "NPPortfolioConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("UPDATE Mentor SET Password=@password" + " WHERE emailAddr = @selectedEmail", conn);

            //Instantiate a DateAdapter object and pass the SqlCommand object
            //created as parameter.
            cmd.Parameters.AddWithValue("@password", password);
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //A connection must be opened before any operations made.
            conn.Open();

            int count = cmd.ExecuteNonQuery();

            //A connection should always be closed, whether error occurs or not.
            conn.Close();


            if(count > 0)
            {
                return 0;
            }
            else
            {
                return -2;
            }
        }

        public int reply()
        {
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("INSERT INTO Reply (MessageID, MentorID, ParentID, DateTimePosted, Text) " + "OUTPUT INSERTED.ReplyID " + "VALUES (@messageID, @mentorID, @parentID, @DateTimePosted, @Text)", conn);

            cmd.Parameters.AddWithValue("@messageID", messageid);
            cmd.Parameters.AddWithValue("@mentorID", mentorid);
            cmd.Parameters.AddWithValue("@parentID", parentid);
            cmd.Parameters.AddWithValue("@DateTimePosted", dt);
            cmd.Parameters.AddWithValue("@Text", Mtext);

            conn.Open();

            int id = (int)cmd.ExecuteScalar();

            conn.Close();
            return id;
        }
        public int add()
        {
            //Read connection string "NPPortfolioConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings
                             ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand
                             ("INSERT INTO Mentor (Name, EmailAddr) " +
                             "OUTPUT INSERTED.MentorID " +
                             "VALUES(@name, @email)", conn);

            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from retrieved from respective class's property.

            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@email", email);


            //A connection to database must be opened before any operations made.
            conn.Open();

            //ExecuteScalar is used to retrieve the auto-generated
            int id = (int)cmd.ExecuteScalar();

            //A connection should be closed after operations.
            conn.Close();

            //Return id when no error occurs.
            return id;
        }

        public bool isEmailExist(string email)
        {
            string strConn = ConfigurationManager.ConnectionStrings
                             ["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand
                            ("SELECT * FROM Mentor WHERE EmailAddr=@selectedEmail", conn);
            cmd.Parameters.AddWithValue("@selectedEmail", email);

            SqlDataAdapter daEmail = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();

            conn.Open();

            //use DataAdapter to fetch data to a table "EmailDetails" in Dataset.
            daEmail.Fill(result, "EmailDetails");
            conn.Close();

            if (result.Tables["EmailDetails"].Rows.Count > 0)
            {
                return true; //the email given exists
            }               
            else
            {
                return false; //the email given does not exist
            }               
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Project
    {
        public int projectId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string projectPoster { get; set; }
        public string projectURL { get; set; }

        public int studentId { get; set; }
        public List<string> members { get; set; }
        public string role { get; set; }
        public string reflection { get; set; }
        int id = 0;
        public int add()
        {
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);

            //Creating portfolio project 
            SqlCommand cmd = new SqlCommand
                ("INSERT INTO Project(Title, Description, ProjectPoster, ProjectURL)" +
                 "OUTPUT INSERTED.ProjectID " +
                "VALUES(@title, @description, @projectposter, @projecturl)", conn);

            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@projectposter", projectPoster);
            cmd.Parameters.AddWithValue("@projecturl", projectURL);
            conn.Open();

            id = (int)cmd.ExecuteScalar();
            projectId = id;
            conn.Close();

            return id;
        }
        public int addMembers()
        {

            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            SqlConnection conn = new SqlConnection(strConn);
            //Adding members in
            SqlCommand cmd2 = new SqlCommand
                ("INSERT INTO ProjectMember(ProjectID,StudentID, Role)" +
                "VALUES(@projectid, @studentid, @role)", conn);
            projectId = id;
            cmd2.Parameters.AddWithValue("@projectid", projectId);
            cmd2.Parameters.AddWithValue("@studentid", studentId);
            cmd2.Parameters.AddWithValue("@role", role);

            conn.Open();

            cmd2.ExecuteNonQuery();

            conn.Close();

            return 0;
        }

        public int updateProject()
        {
            //Read connection string "NPBookConnectionString" from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SQL Statement
            SqlCommand cmd = new SqlCommand
                ("UPDATE Project SET Description=@newDescription,Title=@newTitle,ProjectPoster=@newProjectPoster" +
                ",ProjectURL=@newProjectURL " +
                "WHERE ProjectID=@selectedProjectID", conn);

            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd.Parameters.AddWithValue("@newTitle", title);
            cmd.Parameters.AddWithValue("@newProjectURL", projectURL);
            cmd.Parameters.AddWithValue("@newDescription", description);
            cmd.Parameters.AddWithValue("@newProjectPoster", projectPoster);
            cmd.Parameters.AddWithValue("@selectedProjectID", projectId);

            //Open a Database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE SQL statement/
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs.
            return 0;
        }
        public int updateReflection()
        {
            //Read connection string "NPBookConnectionString" from web.config
            string strConn = ConfigurationManager.ConnectionStrings
                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the connection string read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with a SQL Statement
            SqlCommand cmd = new SqlCommand
                ("UPDATE ProjectMember SET Reflection=@newReflection " +
                "WHERE ProjectID=@selectedProjectID AND StudentID=@selectedStudentID", conn);

            //Define the parameter used in SQL statement, value for the parameter
            //is retrieved from the arguments of the function.
            cmd.Parameters.AddWithValue("@newReflection", reflection);
            cmd.Parameters.AddWithValue("@selectedStudentID", studentId);
            cmd.Parameters.AddWithValue("@selectedProjectID", projectId);

            //Open a Database connection.
            conn.Open();
            //ExecuteNonQuery is used for UPDATE SQL statement/
            cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();

            //Return 0 when no error occurs.
            return 0;
        }
        public int addMembersUpdate()
        {
            string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(sqlConn);
            SqlCommand cmd5 = new SqlCommand("INSERT INTO ProjectMember(ProjectID, StudentID,Role,Reflection) " +
                "VALUES(@projectid,@studentid,@role,@reflection)", conn);
            cmd5.Parameters.AddWithValue("@projectid", projectId);
            cmd5.Parameters.AddWithValue("@studentid", studentId);
            cmd5.Parameters.AddWithValue("@role", role);
            cmd5.Parameters.AddWithValue("@reflection", reflection);

            conn.Open();
            cmd5.ExecuteNonQuery();
            conn.Close();
            return 0;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class Suggestions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                displaySuggestions();
                if (Session["EmailAddr"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
            }

        }
        private void displaySuggestions()
        {
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT SuggestionID,Mentor.Name as Mentor,Description,Status,DateCreated FROM  Suggestion INNER JOIN Mentor ON Suggestion.MentorID " +
                "= Mentor.MentorID WHERE StudentID = '" + Session["StudentID"] +  "'", conn);
            SqlDataAdapter daSuggestion = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daSuggestion.Fill(result, "SuggestionDetails");
            conn.Close();
            gvSuggestions.DataSource = result.Tables["SuggestionDetails"];
            gvSuggestions.DataBind();
        }

        protected void gvSuggestions_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "Acknowledge")
            {
                
                int i = Convert.ToInt32(e.CommandArgument);
                int sId = Convert.ToInt32(gvSuggestions.Rows[i].Cells[0].Text);
                if(gvSuggestions.Rows[i].Cells[3].Text == "Y")
                {
                    lblMsg.ForeColor = System.Drawing.Color.Red;
                    lblMsg.Text = "Suggestion, ID:" + sId.ToString() + " has already been Acknowledged!";
                    return;
                }
                Suggestion objSuggestion = new Suggestion();
                string Status = "Y";
                int errorCode = objSuggestion.update(sId, Status);
                if(errorCode == 0)
                {

                    displaySuggestions();
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                    lblMsg.Text = "Suggestion, ID:" + sId.ToString() + " Acknowledged!"; 
                }



               
            }
            
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ABCTemplate.Master" AutoEventWireup="true" CodeBehind="ViewPortfolio.aspx.cs" Inherits="Assignment1.ViewPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            margin-left: 40px;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-weight: bold;
            font-size: x-large;
            margin-left: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style2">
        View Portfolios</p>
    <p class="auto-style1">
        <asp:GridView ID="gvPortfolio" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="Course" HeaderText="Course" />
                <asp:ImageField DataImageUrlField="Photo" DataImageUrlFormatString="Students/{0}" HeaderText="Photo">
                    <ControlStyle width="50px" />
                </asp:ImageField>
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="Achievement" HeaderText="Achievement" />
                <asp:BoundField DataField="ExternalLink" HeaderText="External Link" />
                <asp:BoundField DataField="EmailAddr" HeaderText="Email Address" />
                <asp:BoundField DataField="Mentor" HeaderText="Mentor" />
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </p>
</asp:Content>

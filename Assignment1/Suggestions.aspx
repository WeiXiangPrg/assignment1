﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="Suggestions.aspx.cs" Inherits="Assignment1.Suggestions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-size: x-large;
            font-weight: bold;
            text-align: center;
            line-height: 150%;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-size: medium;
            text-align: center;
            line-height: 150%;
        }
        .auto-style3 {
            font-size: medium;
            width: 1000px;
            height: 100px;
            left: 15px;
            top: 64px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContentPlaceHolder" runat="server">
    <p class="auto-style1">
        Suggestions</p>
    <div style="margin-left:auto; margin-right:auto;">
        <asp:GridView ID="gvSuggestions" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Height="106px" Width="1110px" OnRowCommand="gvSuggestions_RowCommand" CssClass="auto-style3">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="SuggestionID" HeaderText="ID" />
                <asp:BoundField DataField="Mentor" HeaderText="Mentor" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="Status" HeaderText="Status" />
                <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" />
                <asp:ButtonField ButtonType="Button" CommandName="Acknowledge" Text="Acknowledge" ControlStyle-CssClass="ui primary button" >
                <ControlStyle Height="40px" Width="200px" />
                </asp:ButtonField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
        </div>
    <p class="auto-style2">
        <strong>
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
        </strong>
    </p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlaceHolder" runat="server">
    <p class="text-left">
        <br />
    </p>
</asp:Content>

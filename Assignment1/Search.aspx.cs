﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class Search : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                StudentList();
                DisplayStudentSkill();
            }
             
        }

        private void StudentList()
        {
            
            //Read connection string "NPPorfolioConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object. supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student", conn);

            //Declare and instantiate DataAdapter object
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            conn.Open();
            DataSet result = new DataSet();

            //Use DataAdapter to fetch data to a table "StudentDetails" in DataSet.
            daStudent.Fill(result, "StudentDetails");
            conn.Close();

            //Specify GridView to get data from table "StudentDetails"
            //in DataSet "result"
            gvSearch.DataSource = result.Tables["StudentDetails"];

            //Display the list of data in GridView
            gvSearch.DataBind();
        }

        private void DisplayStudentSkill()
        {

            //Read connection string "NPPortfolioConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object. supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM SkillSet", conn);

            //Declare and instantiate DataAdapter object
            SqlDataAdapter daSkill = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            conn.Open();
            DataSet result = new DataSet();

            //Use DataApadter to fetch data to a table "SkillDetails" in DataSet.
            daSkill.Fill(result, "SkillDetails");
            conn.Close();

            //Specify GridView to get data from table "SkillDetails"
            //in DataSet "result"
            ddlSkillSet.DataSource = result.Tables["SkillDetails"];

            //Specify the Value property of dropdownlist
            ddlSkillSet.DataValueField = "SkillSetName";
            //Display the list of data in GridView
            ddlSkillSet.DataBind();

            //Insert prompt for the DropDownList at the first position of the list
            ddlSkillSet.Items.Insert(0, "--Select--");
        }

        protected void ddlSkillSet_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if(ddlSkillSet.SelectedIndex != 0)
            {
                //Read connection string "NPPortfolioConnectionString" from web.config file.
                string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

                //Instantiate a SqlConnection object with the Connection String read.
                SqlConnection conn = new SqlConnection(strConn);
                conn.Open();
                //Instantiate a SqlCommand object. supply it with the SQL statement
                //SELECT and the connection object used for connecting to the database
                SqlCommand cmd = new SqlCommand("SELECT SkillSet.SkillSetID, SkillSetName, StudentSkillSet.StudentID, StudentSkillSet.SkillSetID, " +
                    "Student.StudentID, Name, Course, Photo, EmailAddr FROM StudentSkillSet INNER JOIN SkillSet ON SkillSet.SkillSetID" +
                    "= StudentSkillSet.SkillSetID INNER JOIN Student ON Student.StudentID = StudentSkillSet.StudentID Where SkillSetName = '" + ddlSkillSet.SelectedValue + "'", conn);
                SqlDataAdapter daSkill = new SqlDataAdapter(cmd);
                DataSet result = new DataSet();
                daSkill.Fill(result, "SkillDetails");

                conn.Close();

                gvSearch.DataSource = result.Tables["SkillDetails"];
                gvSearch.DataBind();
            }
            else
            {
                lblMessage.Text = "Please select valid skill set";
            }
        }
    }
}
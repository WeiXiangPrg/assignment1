﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class Login : System.Web.UI.Page
    {
        private List<string> usertype = new List<string> { "Admin", "Student", "Staff" };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                for (int i = 0; i < usertype.Count; i++)
                {
                    rblUserType.Items.Add(usertype[i].ToString());
                }
                rblUserType.SelectedIndex = 0;
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            string Email = txtEmail.Text;
            string pass = txtPassword.Text;

            string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(sqlConn);
            SqlCommand cmd = new SqlCommand("SELECT * from Mentor Where EmailAddr = '" + txtEmail.Text + "' and Password = '" + txtPassword.Text + "'", conn);
            SqlCommand cmd2 = new SqlCommand("SELECT * from Student Where EmailAddr = '" + txtEmail.Text + "' and Password = '" + txtPassword.Text + "'", conn);
            SqlDataAdapter dalogin = new SqlDataAdapter(cmd);
            SqlDataAdapter dalogin2 = new SqlDataAdapter(cmd2);
            //DataTable dtlog = new DataTable();
            //DataTable dtlog2 = new DataTable();
            DataSet loginID = new DataSet();
            DataSet loginID2 = new DataSet();

            dalogin.Fill(loginID, "StaffLogin");
            dalogin2.Fill(loginID2, "StudentLogin");
            conn.Open();

            if (Email == "admin@ap.edu.sg" && pass == "passAdmin" && rblUserType.SelectedIndex == 0)
            {
                Session["EmailAddr"] = Email;

                Response.Redirect("SystemAdministrator.aspx");
            }
            else if (loginID2.Tables["StudentLogin"].Rows.Count == 1 && rblUserType.SelectedIndex == 1)
            {
                //Display profile, Stores data of Logged in student in the Session
                Session["Photo"] = loginID2.Tables["StudentLogin"].Rows[0]["Photo"];
                Session["Course"] = loginID2.Tables["StudentLogin"].Rows[0]["Course"];
                Session["EmailAddr"] = Email;
                Session["Achievement"] = loginID2.Tables["StudentLogin"].Rows[0]["Achievement"];
                Session["Link"] = loginID2.Tables["StudentLogin"].Rows[0]["ExternalLink"];
                Session["Description"] = loginID2.Tables["StudentLogin"].Rows[0]["Description"];
                Session["StudentID"] = loginID2.Tables["StudentLogin"].Rows[0]["StudentID"];
                Session["Name"] = loginID2.Tables["StudentLogin"].Rows[0]["Name"];
                SqlCommand cmd3 = new SqlCommand("SELECT SkillSetName,StudentSkillSet.SkillSetID FROM SkillSet INNER JOIN StudentSkillSet " +
                    "ON SkillSet.SkillSetID = StudentSkillSet.SkillSetID WHERE StudentID = '" + Session["StudentID"].ToString() + "'", conn);
                SqlDataAdapter daloginSkill = new SqlDataAdapter(cmd3);
                DataSet loginSkill = new DataSet();
                daloginSkill.Fill(loginSkill, "SkillLogin");
                List<string> skillstring = new List<string>();
                for (int i = 0; i < loginSkill.Tables["SkillLogin"].Rows.Count; i++)
                {
                    skillstring.Add(loginSkill.Tables["SkillLogin"].Rows[i]["SkillSetName"].ToString());
                }
                string Skills = string.Join(",", skillstring.ToArray());
                Session["SkillSetName"] = Skills;

                //Update profile set of code required 
                //Populating check box list by obtaining all of the skillsetid and skillsetnames
                SqlCommand cmd4 = new SqlCommand("SELECT * FROM SkillSet", conn);
                SqlDataAdapter daSkillTable = new SqlDataAdapter(cmd4);
                DataSet skillTable = new DataSet();
                daSkillTable.Fill(skillTable, "SkillTable");
                List<int> skillID = new List<int>();
                List<string> skillName = new List<string>();
                for (int i = 0; i < skillTable.Tables["SkillTable"].Rows.Count; i++)
                {
                    skillID.Add(Convert.ToInt32(skillTable.Tables["SkillTable"].Rows[i]["SkillSetID"]));
                    skillName.Add(skillTable.Tables["SkillTable"].Rows[i]["SkillSetName"].ToString());
                }
                string skillListID = string.Join(",", skillID.ToArray());
                Session["SkillIDList"] = skillListID;
                string skillListName = string.Join(",", skillName.ToArray());
                Session["SkillNameList"] = skillListName;
                Response.Redirect("Student.aspx");
            }
            else if (loginID.Tables["StaffLogin"].Rows.Count == 1 && rblUserType.SelectedIndex == 2)
            {

                Session["EmailAddr"] = Email;
                Session["MentorID"] = loginID.Tables["StaffLogin"].Rows[0]["MentorID"];
                Session["Name"] = loginID.Tables["StaffLogin"].Rows[0]["Name"];
                Response.Redirect("Mentor.aspx");
            }
            else
            {
                lblMessage.Text = "Invalid Login Credentials";
            }
            conn.Close();


        }
        /*if (loginName == "peter ghim" && pass == "p@55Mentor" && usertype == "Staff")
        {
            Session["LoginName"] = loginName.ToUpper();

            Response.Redirect("Mentor.aspx");
        }
        else if (loginName == "admin@ap.edu.sg" && pass == "passAdmin" && usertype == "Admin")
        {
            Session["loginName"] = loginName;

            Response.Redirect("SystemAdministrator.aspx");
        }
        else if (loginName == Request.QueryString["Name"] && pass == Request.QueryString["Password"] && usertype == "Student")
        {
            Session["loginName"] = loginName;

            Response.Redirect("Student.aspx");
        }
        else
        {
            lblMessage.Text = "Invalid Login Credentials";
        }
    }*/
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="UpdatePort.aspx.cs" Inherits="Assignment1.UpdatePort" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: Arial;
            font-weight: bold;
            font-size: medium;
        }
        .auto-style2 {
            font-family: Arial;
            font-weight: bold;
            font-size: large;
        }
        .auto-style3 {
            font-family: Arial;
            font-size: small;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeftContentPlaceHolder" runat="server">
    <p class="auto-style2">
        Update Portfolios and Reflection</p>
    <p>
        <span class="auto-style3"><strong>Project Selected ID:</strong>
        <asp:Label ID="lblID" runat="server"></asp:Label></p>
    <p>
        <strong>Current Name:</strong> 
        <asp:Label ID="lblName" runat="server"></asp:Label>
        </span>
    </p>
    <div class ="ui form">
        <div class="field">
            <label>Project Name</label>
        <asp:TextBox ID="txtName" runat="server" Height="30px" Width="200px" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter a Name" ForeColor="Red" CssClass="auto-style6">*</asp:RequiredFieldValidator>
        </div>
        </div>
    <p>
        <div class="ui form">
            <div class="field">
                  <label>Description</label>
                 <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Height="150px" Width="600px"></asp:TextBox>
            </div>
        </div>
    </p>
    <div class="ui form">
        <div class="field">
            <label>Project URL</label>
            <asp:TextBox ID="txtURL" runat="server" Height="30px" Width="550px" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
        </div>
    </div>
    </br>
    <p>
        <span class="auto-style6">Upload Poster Image :
        </span>
        <asp:FileUpload ID="upPoster" runat="server" CssClass="auto-style6" />
        <span class="auto-style6">
        <asp:RegularExpressionValidator ID="revUpload" runat="server" ControlToValidate="upPoster" Display="Dynamic" ErrorMessage="Only Images formats allowed." ForeColor="Red" ValidationExpression="(.*png$)|(.*jpg$)|(.*jpeg$)"></asp:RegularExpressionValidator>
    </p>
    <p>
        <asp:Image ID="imgPhoto" runat="server" Height="100px" Width="100px" />
        <span class="auto-style6">
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
    </span>
    </p>
    <p>
        <div class ="ui form">
            <div class="field">
                 <label>Reflection</label>
                 <asp:TextBox ID="txtReflection" runat="server" TextMode="MultiLine" Height="150px" Width="600px"></asp:TextBox>
            </div>
        </div>

    </p>
    <p>
        <strong>Members :</strong><asp:Label ID="lblMembers" runat="server"></asp:Label>
        <div class="ui checkbox">
            <asp:CheckBoxList ID="cblMembers" runat="server" Height="40px" RepeatDirection="Horizontal" Width="460px" CellPadding="0" CellSpacing="0">
        </asp:CheckBoxList>
        </div>
        
        </p>
    <p>
        <div class="ui form">
            <div class="field">
                 <label><asp:Label ID="lblAdd" runat="server"></asp:Label></label>
             <asp:TextBox ID="txtAdd" runat="server" Width="300px" CssClass="auto-style6" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
            </div>
        </div>
        </p>
    <p>
        <strong>
        <asp:Label ID="lblAdd2" runat="server" CssClass="auto-style1" ForeColor="Red"></asp:Label>
        </strong>
        </p>
    <p>
        <asp:Button ID="btnConfirm" runat="server" Text="Update" OnClick="btnConfirm_Click" CssClass="ui green button" />
        <span class="auto-style6">
        </br>
            <strong>
        <asp:Label ID="lblCfm" runat="server" ForeColor="Green"></asp:Label>
        </strong>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        </span></p>
    </span>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlaceHolder" runat="server">
</asp:Content>

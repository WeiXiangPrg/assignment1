﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class ChangePass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                lblEmail.Text = (string)Session["EmailAddr"];
            }         
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Mentors objMentor = new Mentors();

                objMentor.email = (string)Session["EmailAddr"];

                objMentor.password = txtCfmNewPass.Text;

                int errorCode = objMentor.update();

                if (errorCode == 0)
                {
                    lblMessage.Text = "Password has been updated successfully.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtNewPass.Text = "";
            txtCfmNewPass.Text = "";
            
        }
    }
}
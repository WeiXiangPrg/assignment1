﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class ApproveViewingRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                displayViewingRequestList();
            }
        }

        private void displayViewingRequestList()
        {
            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings
                ["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlConnection object, supply it with the SQL statement 
            //Select and the connection object used for connecting to the database.
            SqlCommand cmd = new SqlCommand("Select * From ViewingRequest Where Status = 'P' Order By ViewingRequestID", conn);

            //Declare and instantiate DataAdapter object 
            SqlDataAdapter daRequest = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from database
            DataSet result = new DataSet();

            conn.Open();


            //Use DataAdapter to fetch data to a table "BranchDetails" in DataSet.
            daRequest.Fill(result, "ViewingRequest");
            conn.Close();


            //Specify GridView to get data from table "BranchDetails"
            //In Dataset "result"
            gvApprove.DataSource = result.Tables["ViewingRequest"];
            //Display the list of data in gridview

            gvApprove.DataBind();//display

        }

        protected void gvApprove_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ApproveRequest")
            {
                //Retrieve the row index stored in the CommandArgument property
                int i = Convert.ToInt32(e.CommandArgument);

                SystemAdmin objSystem = new SystemAdmin();

                objSystem.requestId = Convert.ToInt32(gvApprove.Rows[i].Cells[0].Text);

                int errorCode = objSystem.updateStatus("A");
                gvApprove.DataBind();

            }
            else if (e.CommandName == "RejectRequest")
            {
                //Retrieve the row index stored in the CommandArgument property
                int i = Convert.ToInt32(e.CommandArgument);

                SystemAdmin objSystem = new SystemAdmin();

                objSystem.requestId = Convert.ToInt32(gvApprove.Rows[i].Cells[0].Text);

                int errorCode = objSystem.updateStatus("R");
                gvApprove.DataBind();
            }




        }

    }
}
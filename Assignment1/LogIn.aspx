﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ABCTemplate.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Assignment1.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <h1 style="text-align:center;">
        Login
    </h1>
  <asp:Panel ID="Panel1" runat="server" DefaultButton="btnLogin" > 

       <div style="text-align:center" >
            <asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Please fill in a valid email" ForeColor="Red" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                <br />
        <div class="ui left icon input">
                    <i class="user icon"></i>
      <asp:TextBox ID="txtEmail" runat="server" placeholder="Email-address" CausesValidation="True"></asp:TextBox>
                    </div>
     <p class="text-center">
            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please fill in an email" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <div class="ui left icon input">
            <i class="lock icon"></i>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" placeholder="Password" CausesValidation="True"  ></asp:TextBox>
        </div>
            <p class="text-center">
            <asp:RequiredFieldValidator ID="rfvPass" runat="server" ControlToValidate="txtPassword" Display="Dynamic" ErrorMessage="Please fill in password" ForeColor="Red"></asp:RequiredFieldValidator>
        </p>
        <div class="field">
            <div class="ui radio checkbox">
                     <asp:RadioButtonList ID="rblUserType" runat="server" RepeatDirection="Horizontal">
            </asp:RadioButtonList>
            </div>
        </div>
        <p >
            <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" CssClass="ui teal button" Width="150px" />
            </br>
        </p>
     <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>

    </div>
  </asp:Panel>
  
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="UpdateProfile.aspx.cs" Inherits="Assignment1.UpdateProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            line-height: 150%;
            text-align: justify;
            font-family: Arial;
            font-size: x-large;
        }
        .auto-style3 {
            font-family: "Arial";
            font-size: small;
        }
        .auto-style4 {
            text-align: justify;
            font-family: Arial;
            font-size: small;
        }
        .auto-style5 {
            font-size: small;
        }
        .auto-style6 {
            font-size: 0.928571em;
            font-family: Arial;
            font-weight: bold;
        }
        .auto-style7 {
            font-family: "Arial";
            font-size: 0.928571em;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContentPlaceHolder" runat="server">
    <p class="auto-style1">
        <strong>Update Profile</strong></p>
    <p class="auto-style4">
        <strong>Current E-mail :
        </strong>
        <asp:Label ID="lblEmail" runat="server"></asp:Label>
    </p>
        
            <span class="auto-style5"><strong>New E-mail </strong>&nbsp;&nbsp;<br />
            </span>
    <div class ="ui input">
        <asp:TextBox ID="txtEmail" runat="server" CssClass="ui input"  onkeydown = "return (event.keyCode!=13);" ></asp:TextBox>
        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic"  ErrorMessage="Please enter a valid email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
 </div>
 <p></p>
    <div class ="ui form">
        <div class ="field">
            <label>Description</label>
                <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Height="150px" Width="850px"></asp:TextBox>
    
        </div>
        </div>
    <p></p>
    <div class="ui form">
           <div class="field">
               <label>Achievements</label>
        <asp:TextBox ID="txtAch" runat="server" TextMode="MultiLine" Height="100px" Width="850px"></asp:TextBox>
    
               <br />
        </div>
        </div>
    <p></p>
    <p>
        <span class="auto-style6"> Personal Link   </span>
            </br>
        </p>
             <div class="ui input">
            
            <asp:TextBox ID="txtLink" runat="server" Width="564px" CssClass="auto-style5" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
               </div>
            
    <p></p>
        <div class="text-justify">
            <strong>
            <span class="auto-style7">Skills</span></strong></br>
         <div class="ui checkbox">
            <asp:CheckBoxList ID="cblSkills" runat="server" RepeatDirection="Horizontal" Height="40px" Width="1110px">
            </asp:CheckBoxList>
        </div>
             </div>
    </p>
    <p class="text-justify">
&nbsp;<strong>Profile Picture :</strong>
        </span>
        <asp:FileUpload ID="upPhoto" runat="server" Width="259px" CssClass="auto-style5" />
        &nbsp;<span class="auto-style5"><asp:RegularExpressionValidator ID="revUpload" runat="server" ControlToValidate="upPhoto" Display="Dynamic" ErrorMessage="Only Images formats allowed." ForeColor="Red" ValidationExpression="(.*png$)|(.*jpg$)|(.*jpeg$)"></asp:RegularExpressionValidator>
    </p>
    <p class="text-justify">
        <strong>Preview:</strong> 
        <asp:Image ID="imgPhoto" runat="server" Height="100px" Width="100px"  />
    </p>
    <p class="text-justify">
        </span>
    </p>
    <p aria-atomic="True" class="text-justify">
        <asp:Button ID="btnConfirm" runat="server" CausesValidation="False" Text="Update Profile" OnClick="btnConfirm_Click" CssClass="ui green button" />
&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="auto-style5"></asp:Label>
        <strong>
        <asp:Label ID="lblError" runat="server" CssClass="auto-style3" ForeColor="Red"></asp:Label>
        </strong>
    </p>
    <p aria-atomic="True" class="text-justify">
   
           <asp:Label ID="lblCfm" runat="server" CssClass="auto-style3" ForeColor="Green" ></asp:Label>
        
       
    </p>
    <p class="auto-style1">
        &nbsp;</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlaceHolder" runat="server">
    <p aria-atomic="True" class="text-justify">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </p>
</asp:Content>

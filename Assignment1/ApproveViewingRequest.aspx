﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AdminTemplate.Master" AutoEventWireup="true" CodeBehind="ApproveViewingRequest.aspx.cs" Inherits="Assignment1.ApproveViewingRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
             font-size: x-large;
            font-weight: bold;
            width: 299px;
            height: 48px;
        }
        .auto-style3 {
            height: 48px;
        }
        .auto-style4 {
            width: 299px;
            height: 45px;
        }
             
        .auto-style6 {
            width: 100%;
            margin-bottom: 0px;
        }
             
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style6">
        <tr>
            <td class="auto-style2">Approve Viewing Request</td>
            <td class="auto-style3"></td>
        </tr>
        <tr>
            <td class="auto-style4">
                <asp:GridView ID="gvApprove" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gvApprove_RowCommand" Width="1001px" >
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="ViewingRequestID" HeaderText="ViewingRequestID" />
                        <asp:BoundField DataField="ParentID" HeaderText="ParentID" />
                        <asp:BoundField DataField="StudentName" HeaderText="StudentName" />
                        <asp:BoundField DataField="StudentID" HeaderText="StudentID" />
                        <asp:BoundField DataField="Status" HeaderText="Status" />
                        <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" />
                        <asp:ButtonField ButtonType="Button" CommandName="ApproveRequest" Text="Approve" ControlStyle-CssClass="ui positive button" />
                        <asp:ButtonField ButtonType="Button" CommandName="RejectRequest" Text="Reject"  ControlStyle-CssClass="ui negative button"/>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
        </tr>
        </table>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class CreatePortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtLeader.Text = Session["Name"].ToString();
                if (Session["EmailAddr"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Project objproject = new Project();
                objproject.title = txtTitle.Text;
                objproject.description = txtDescription.Text;
                objproject.projectURL = txtURL.Text;
                string uploadedFile = "";
                if (upPoster.HasFile == true)
                {
                    string savePath;

                    //Find the filename extension of the file to be uploaded.
                    string fileExt = Path.GetExtension(upPoster.FileName);
                    uploadedFile = upPoster.FileName;
                    objproject.projectPoster = upPoster.FileName;
                    //MapPath - to fins the complete path to the images folder in server
                    savePath = MapPath("~/Projects/" + uploadedFile);

                    try
                    {
                        upPoster.SaveAs(savePath); //Upload the file to server
                        lblMsg.Text = "File uploaded successfully.";
                        imgPoster.ImageUrl = "~/Projects/" + uploadedFile;

                    }
                    catch (IOException)
                    {
                        //File IO error, could due to access rights denied
                        lblMsg.Text = "File uploading fail!";

                    }
                    catch (Exception ex) // other type of error
                    {
                        lblMsg.Text = ex.Message;
                    }
                }
                int id = objproject.add();
                lblCfm.Text = "Succesfully created an Project Portfolio";
                //Adding of leader
                objproject.role = "Leader";
                objproject.studentId = Convert.ToInt32(Session["StudentID"]);
                objproject.addMembers();
                //Adding of members
                List<string> memberList = txtMembers.Text.Split(',').ToList();
                foreach (string member in memberList)
                {
                    string sqlConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
                    SqlConnection conn = new SqlConnection(sqlConn);
                    SqlCommand cmd = new SqlCommand("SELECT StudentID FROM Student WHERE NAME = '" + member + "'", conn);
                    SqlDataAdapter daStudent = new SqlDataAdapter(cmd);
                    DataSet StudentID = new DataSet();
                    daStudent.Fill(StudentID, "StudentID");
                    objproject.studentId = Convert.ToInt32(StudentID.Tables["StudentID"].Rows[0]["StudentID"]);
                    objproject.role = "Member";
                    objproject.addMembers();

                }
            }
        }
    }
}
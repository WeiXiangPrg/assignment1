﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class ReplyForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack)
            {
                DisplayParentID();
            }
        }

        protected void btnReply_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                if (ddlParentName.SelectedIndex != 0)
                {
                    DateTime dTime = DateTime.Now;
                    lblDT.Text = dTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                    Mentors objMentor = new Mentors();

                    objMentor.messageid = Convert.ToInt32(Request.QueryString["MessageID"]);
                    lblMessage.Text = Request.QueryString["MessageID"];
                    objMentor.mentorid = Convert.ToInt32(Session["MentorID"]);
                    lblMentorName.Text = Session["Name"].ToString();
                    objMentor.parentid = Convert.ToInt32(ddlParentName.SelectedValue);
                    objMentor.dt = Convert.ToDateTime(lblDT.Text);
                    objMentor.Mtext = taMessage.Value;

                    int id = objMentor.reply();

                    lblReply.Text = id.ToString();
                    lblPN.Visible = false;
                }
                else
                {
                    lblPN.Text = "Please select valid parent's name";
                    lblPN.Visible = true;
                }
            }
        }

        private void DisplayParentID()
        {

            //Read connection string "NPPortfolioConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object. supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Parent", conn);

            //Declare and instantiate DataAdapter object
            SqlDataAdapter daSkill = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            conn.Open();
            DataSet result = new DataSet();

            //Use DataApadter to fetch data to a table "ParentIDDetails" in DataSet.
            daSkill.Fill(result, "ParentIDDetails");
            conn.Close();

            //Specify GridView to get data from table "ParentIDDetails"
            //in DataSet "result"
            ddlParentName.DataSource = result.Tables["ParentIDDetails"];

            //Specify the Value property of dropdownlist
            ddlParentName.DataValueField = "ParentID";
            ddlParentName.DataTextField = "ParentName";
            //Display the list of data in GridView
            ddlParentName.DataBind();

            //Insert prompt for the DropDownList at the first position of the list
            ddlParentName.Items.Insert(0, "--Select--");
        }
    }
}
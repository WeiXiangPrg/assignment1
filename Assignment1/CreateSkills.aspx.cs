﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class CreateSkills : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConfirm_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Students objStudent = new Students();

                objStudent.skill = txtSkillset.Text;

                //Call the add method to insert the Skills record to database.
                int id = objStudent.addskills();

                lblSkills.Text = id.ToString();

                lblMessage.Text = "SkillSet have been added successfully.";


            }

        }

        protected void cuvSkillSet_ServerValidate(object source, ServerValidateEventArgs args)
        {

            if (Page.IsValid) //other client-side validation passed
            {
                Students objStudent = new Students();

                if (objStudent.isSkillsExist(txtSkillset.Text) == true)
                    args.IsValid = false; //Raise error
                else
                    args.IsValid = true; //No error
            }

        }
    }
}
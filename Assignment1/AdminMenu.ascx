﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdminMenu.ascx.cs" Inherits="Assignment1.AdminMenu" %>
<!-- A grey navbar that expands horizontally at medium device -->
            <nav class="navbar  navbar-expand-xl navbar-light bg-light">
    <!-- The brand(or icon) of the navbar -->
    <a class="navbar-brand" href="SystemAdministrator.aspx"
        style="font-size:32px; font-weight:bold; color:#808080;">
        ABC Polytechnic
    </a>
    <!-- Toggle/collpsible Button, also known as hamburger button -->
   <button class="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#ABCNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

                
    <!--Links in the navbar, displayed as drop-down list when collasped -->
    <div class="collapse navbar-collapse" id="ABCNavbar">
        <!-- Links that are aligned to the left, 
            mr-auto: right margin auto-adjudted -->

        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="CreateMentor.aspx">Create New Mentor</a>

            </li>
            <li class="nav-item">
                <a class="nav-link" href="CreateStudent.aspx">Create New Student</a>

                
            </li>
            <li class="nav-item">
                <a class="nav-link" href="CreateSkills.aspx">Choices of Skill Set</a>

                
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ApproveViewingRequest.aspx">Approve Viewing Request</a>

           

          
        </ul>
        <!-- Links that are aligned to the right, ml-auto: left margin auto-adjusted -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- A Web form control button for logging our user -->
                    <i class="power off icon"></i>
                   <asp:Button ID="btnLogOut" runat="server" Text="Log Out"  CausesValidation="false"
                CssClass="negative ui button" OnClick="btnLogOut_Click"/>
            </li>
         
        </ul>
    </div>
</nav>
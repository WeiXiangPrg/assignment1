﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if(Session["EmailAddr"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                imgProfile.ImageUrl = "~/Students/" + Session["Photo"].ToString();
                lblName.Text = Session["Name"].ToString();
                lblID.Text = Session["StudentID"].ToString();
                lblCourse.Text = Session["Course"].ToString();
                lblAch.Text = Session["Achievement"].ToString();
                lblLink.Text = "<a href=" + Session["Link"].ToString() + ">"+ Session["Link"].ToString() + "</a>";
                lblEmail.Text = Session["EmailAddr"].ToString();
                lblDesc.Text = Session["Description"].ToString();
                List<string> skillSet = Session["SkillSetName"].ToString().Split(',').ToList();

                lblSkills.Text = string.Join(" | ", skillSet.ToArray());


            }
        }

        protected void Update_Click(object sender, EventArgs e)
        {
            Response.Redirect("UpdateProfile.aspx");
            
        }
    }
}
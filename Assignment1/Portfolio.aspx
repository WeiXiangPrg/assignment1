﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="Portfolio.aspx.cs" Inherits="Assignment1.Portfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: left;
            font-family: "Segoe UI";
            font-size: large;
        }
        .auto-style2 {
            margin-right: 0px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftContentPlaceHolder" runat="server">
    <p class="auto-style1">
        <strong>Portfolio</strong></p>
        <asp:GridView ID="gvPortfolio" runat="server" AllowCustomPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Height="400px" Width="1110px" CssClass="auto-style2">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:BoundField DataField="ProjectID" HeaderText="ID" />
                <asp:BoundField DataField="Title" HeaderText="Title" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="Projects/{0}" HeaderText="ProjectPoster" ControlStyle-Width="200" ControlStyle-Height = "350">
<ControlStyle Height="250px" Width="150px"></ControlStyle>
                </asp:ImageField>
                <asp:BoundField DataField="ProjectURL" HeaderText="ProjectURL" />
                <asp:HyperLinkField DataNavigateUrlFields="ProjectId,Title"
                    DataNavigateUrlFormatString="UpdatePort.aspx?projectid={0}&amp;title={1}" Text="Update" ControlStyle-CssClass="ui primary button" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightContentPlaceHolder" runat="server">
    <p>
        <br />
    </p>
</asp:Content>

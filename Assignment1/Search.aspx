﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="Assignment1.Search" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
        }
        .auto-style2 {
            font-size: medium;
        }
        .auto-style3 {
            font-size: x-large;
            font-family: "Segoe UI";
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="auto-style3">
        <strong>Search</strong></p>
    <p class="auto-style1">
        <span class="auto-style2">&nbsp; &nbsp; &nbsp;</span><br class="auto-style2" />
        <div class=" ui form">
            <div class="field">
                    <asp:DropDownList ID="ddlSkillSet" runat="server" OnSelectedIndexChanged="ddlSkillSet_SelectedIndexChanged" CssClass="ui dropdown" Width="300px">
        </asp:DropDownList>
            </div>
        </div>
        <br />
        
        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" CssClass="ui blue button" style="font-size: medium" />
        <span class="auto-style2">
        <br />
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
        <br />
        </span>
        <asp:GridView ID="gvSearch" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="auto-style2" Width="1076px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="StudentID" HeaderText="Student ID" />
                <asp:BoundField DataField="Name" HeaderText="Name" />
                <asp:BoundField DataField="Photo" HeaderText="Photo" Visible="False" />
                <asp:ImageField DataImageUrlField="Photo" DataImageUrlFormatString="Students/{0}" HeaderText="Photo">
                    <ControlStyle Width="50px" />
                    <FooterStyle Width="50px" />
                </asp:ImageField>
                <asp:BoundField DataField="Course" HeaderText="Course" />
                <asp:BoundField DataField="EmailAddr" HeaderText="Email Address" />
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
    </p>
    <p class="auto-style2">
        <br />
        
        <br />
    </p>
</asp:Content>

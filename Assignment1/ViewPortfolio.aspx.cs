﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class ViewPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StudentList();
        }

        private void StudentList()
        {

            //Read connection string "NPBookConnectionString" from web.config file.
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object. supply it with the SQL statement
            //SELECT and the connection object used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT Student.Name,Course,Photo,Description,Achievement," +
                "ExternalLink,Student.EmailAddr,Mentor.Name as Mentor FROM Student INNER JOIN Mentor ON " +
                    " Student.MentorID = Mentor.MentorID WHERE  Status = 'Y'" , conn);
            //testing

            //Declare and instantiate DataAdapter object
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);


            //Create a DataSet object to contain the records retrieved from database
            conn.Open();
            DataSet result = new DataSet();

            //Use DataAdapter to fetch data to a table "StudentDetails" in DataSet.
            daStudent.Fill(result, "StudentDetails");
            conn.Close();

            //Specify GridView to get data from table "StudentDetails"
            //in DataSet "result"
            gvPortfolio.DataSource = result.Tables["StudentDetails"];

            //Display the list of data in GridView
            gvPortfolio.DataBind();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class Student : System.Web.UI.Page
    {
        public Student()
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["EmailAddr"] != null)
                {

                    lblWelcome.Text = "Welcome, " + (string)Session["Name"];
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class Message : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                displayMessageList();
            }
        }

        private void displayMessageList()
        {
            //Read connection string "NPPortfolioConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database.
            SqlCommand cmd = new SqlCommand("SELECT MessageID, Parent.ParentName, Mentor.Name, DateTimePosted, Text, MentorID, Mentor.EmailAddr FROM Message INNER JOIN Mentor ON ToID = MentorID INNER JOIN Parent ON FromID = ParentID Where Mentor.EmailAddr = '" + (string)Session["EmailAddr"] + "'", conn);

            SqlDataAdapter daMessage = new SqlDataAdapter(cmd);

            //Create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operations made.
            conn.Open();

            //Use DateAdapter to fetch data to a table "ReplyDetails" in DataSet.
            //DataSet "result" will store the result of the SELECT operation.
            daMessage.Fill(result, "ReplyDetails");

            //A connection should always be closed, whether error occurs or not.
            conn.Close();

                //Specify GridView to get data from table "ReplyDetails"
                //in DataSet "result"
                gvMessage.DataSource = result.Tables["ReplyDetails"];

                //Display the list of data in GridView
                gvMessage.DataBind();
            
        }

        protected void gvMessage_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
    }
}
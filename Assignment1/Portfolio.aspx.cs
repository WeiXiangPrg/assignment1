﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Assignment1
{
    public partial class Portfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                displayPortfolios();
            }
           

        }

        private void displayPortfolios()
        {
            string strConn = ConfigurationManager.ConnectionStrings["NPPortfolioConnectionString"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT Project.ProjectID, Title, Description, ProjectPoster, ProjectURL FROM Project INNER JOIN ProjectMember" +
                " on Project.ProjectID = ProjectMember.ProjectID WHERE StudentID = '" + Session["StudentID"] + "'", conn);
            SqlDataAdapter daPortfolio = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daPortfolio.Fill(result, "ProjectDetails");
            conn.Close(); 
            gvPortfolio.DataSource = result.Tables["ProjectDetails"];
            gvPortfolio.DataBind();
        }
    }
}